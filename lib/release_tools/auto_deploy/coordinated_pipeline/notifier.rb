# frozen_string_literal: true

module ReleaseTools
  module AutoDeploy
    module CoordinatedPipeline
      class Notifier
        include ReleaseTools::AutoDeploy::Pipeline
        include ::SemanticLogger::Loggable

        # pipeline_id    - Pipeline id from release/tools in Ops
        # deploy_version - Deployer package
        # environment    - gstg-cny, gstg, gprd-cny, gprd
        def initialize(pipeline_id:, deploy_version:, environment:)
          @pipeline_id    = pipeline_id
          @deploy_version = deploy_version
          @environment    = environment
        end

        def execute
          return unless Feature.enabled?(:single_pipeline_phase_2_slack_notification)

          pipeline = find_downstream_pipeline

          if pipeline
            logger.info('Downstream pipeline found', deployer_url: pipeline.web_url)

            send_slack_notification(pipeline)
          else
            logger.fatal('Downstream pipeline not found')
          end
        end

        private

        attr_reader :pipeline_id, :deploy_version, :environment

        def find_downstream_pipeline
          logger.info('Fetching downstream pipeline', environment: environment, deploy_version: deploy_version, pipeline_id: pipeline_id)

          bridges = find_bridges

          bridges
            .find { |job| job.name == "auto_deploy:deploy:#{environment}" }
            &.downstream_pipeline
        rescue MissingPipelineError
          nil
        end

        def find_bridges
          bridges = Retriable.with_context(:pipeline_created) do
            logger.debug('Finding bridges for pipeline', pipeline_id: pipeline_id)

            ReleaseTools::GitlabOpsClient.pipeline_bridges(Project::ReleaseTools, pipeline_id)
          end

          raise MissingPipelineError if bridges.empty?

          bridges
        end

        def send_slack_notification(deployer_pipeline)
          ReleaseTools::Slack::CoordinatedPipelineNotification.new(
            deploy_version: deploy_version,
            pipeline: deployer_pipeline,
            environment: environment
          ).execute
        end
      end
    end
  end
end
