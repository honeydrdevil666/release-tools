# frozen_string_literal: true

module ReleaseTools
  module Rollback
    # Presents a Validator instance in GitLab Markdown or Slack format
    class Presenter
      include ::SemanticLogger::Loggable

      LINK_FORMAT = 'https://gitlab.com/gitlab-org/security/gitlab/-/blob/%<commit>s/%<file>s'

      Link = Struct.new(:prefix, :text, :url) do
        def to_s
          to_markdown
        end

        def to_markdown
          "* #{prefix}[`#{text}`](#{url})"
        end

        def to_slack
          "• #{prefix}<#{url}|`#{text}`>"
        end
      end

      # comparison - Rollback::Comparison instance
      def initialize(comparison)
        @comparison = comparison
      end

      def present
        lines = [
          "*Current:* `#{@comparison.current_package}`\n",
          "*Target:* `#{@comparison.target_package}`\n"
        ]

        lines << "*Comparison:* #{@comparison.web_url}\n"
        lines << ":warning: Comparison timed out\n" if @comparison.timeout?
        lines << ":warning: Comparison was empty\n" if @comparison.empty?

        lines.push(*post_deploy_status)
        lines.push(*migration_status)

        lines
      end

      private

      def blob_url(file)
        format(LINK_FORMAT, commit: @comparison.current_rails_sha, file: file)
      end

      def post_deploy_status
        return if @comparison.post_deploy_migrations.none?

        status = [":new: #{@comparison.post_deploy_migrations.size} post-deploy migrations\n"]

        @comparison.post_deploy_migrations.each do |migration|
          text = migration['new_path'].delete_prefix('db/post_migrate/')
          status << Link.new('Post-deploy: ', text, blob_url(migration['new_path']))
        end

        status
      end

      def migration_status
        return if @comparison.migrations.none?

        status = [":new: #{@comparison.migrations.size} migrations\n"]

        @comparison.migrations.each do |migration|
          text = migration['new_path'].delete_prefix('db/migrate/')
          status << Link.new('Migration: ', text, blob_url(migration['new_path']))
        end

        status
      end
    end
  end
end
