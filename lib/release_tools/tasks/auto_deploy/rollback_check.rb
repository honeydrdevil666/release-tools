# frozen_string_literal: true

module ReleaseTools
  module Tasks
    module AutoDeploy
      class RollbackCheck
        def initialize
          @current = ENV.fetch('ROLLBACK_CURRENT')
          @target = ENV.fetch('ROLLBACK_TARGET')

          @compare = Rollback::CompareService.new(current: @current, target: @target)
        end

        def execute
          @comparison = @compare.execute
          @presenter = Rollback::Presenter.new(@comparison)

          blocks = slack_blocks.as_json

          Retriable.retriable do
            ReleaseTools::Slack::Message.post(
              channel: ENV.fetch('CHAT_CHANNEL', ReleaseTools::Slack::F_UPCOMING_RELEASE),
              message: "Rollback check results",
              blocks: blocks
            )
          end
        end

        private

        def slack_blocks
          blocks = ::Slack::BlockKit.blocks

          blocks.header(text: header, emoji: true)

          blocks.section do |block|
            lines = @presenter.present.map do |line|
              line.try(:to_slack) || line
            end

            block.mrkdwn(text: lines.join("\n"))
          end

          blocks.context { |c| c.mrkdwn(text: handbook_link) }

          blocks
        end

        def header
          if @comparison.safe?
            ":large_green_circle: Rollback available:"
          else
            ":red_circle: Rollback unavailable:"
          end
        end

        def handbook_link
          url = 'https://gitlab.com/gitlab-org/release/docs/-/blob/master/runbooks/rollback-a-deployment.md'

          ":book: <#{url}|View runbook>"
        end
      end
    end
  end
end
