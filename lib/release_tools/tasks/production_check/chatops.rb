# frozen_string_literal: true

module ReleaseTools
  module Tasks
    module ProductionCheck
      # Perform a production health check via ChatOps
      #
      # Results are posted to a specified channel.
      class Chatops
        include ::SemanticLogger::Loggable

        UnsafeProductionError = Class.new(StandardError)

        def initialize
          @channel = ENV.fetch('CHAT_CHANNEL')
        end

        def execute
          notify_on_slack(status.to_slack_blocks)

          return if status.fine?

          raise UnsafeProductionError if raise_on_failure?
        end

        def notify_on_slack(blocks)
          return if SharedStatus.dry_run?

          Retriable.retriable do
            Slack::ChatopsNotification.fire_hook(channel: @channel, blocks: blocks)
          end
        rescue ReleaseTools::Slack::Webhook::CouldNotPostError
          logger.error('Malformed Slack request', channel: @channel, blocks: blocks.to_json)

          raise
        end

        private

        def raise_on_failure?
          ENV['FAIL_IF_NOT_SAFE'] == 'true'
        end

        def status
          @status ||= Promotion::ProductionStatus.new(*checks)
        end

        def checks
          if ENV.key?('SKIP_DEPLOYMENT_CHECK')
            %i[canary_up]
          else
            %i[canary_up active_deployments]
          end
        end
      end
    end
  end
end
