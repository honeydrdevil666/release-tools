# frozen_string_literal: true

module ReleaseTools
  module Slack
    class CoordinatedPipelineTagNotification
      def initialize(deploy_version:, auto_deploy_branch:, tag_version:)
        @deploy_version = deploy_version
        @auto_deploy_branch = auto_deploy_branch
        @tag_version = tag_version
      end

      def execute
        ReleaseTools::Slack::Message.post(
          channel: ReleaseTools::Slack::F_UPCOMING_RELEASE,
          message: coordinated_pipeline_message,
          mrkdwn: true
        )
      end

      private

      attr_reader :deploy_version, :auto_deploy_branch, :tag_version

      def coordinated_pipeline_message
        "New coordinated pipeline: #{coordinated_pipeline_link} (#{auto_deploy_branch_link} @ `#{rails_sha}`)"
      end

      def coordinated_pipeline_link
        "<#{ENV['CI_PIPELINE_URL']}|#{tag_version}>"
      end

      def auto_deploy_branch_link
        "<#{security_branch_url}|#{auto_deploy_branch}>"
      end

      def security_branch_url
        "https://gitlab.com/gitlab-org/security/gitlab/-/commits/#{auto_deploy_branch}"
      end

      def rails_sha
        ReleaseTools::AutoDeploy::Version.new(deploy_version).rails_sha
      end
    end
  end
end
