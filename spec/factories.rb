# frozen_string_literal: true

FactoryBot.define do
  sequence(:id) { |n| n }
  sequence(:sha) { SecureRandom.hex(20) }
  sequence(:created_at, aliases: %w[updated_at finished_at]) do
    Time.now.utc.iso8601
  end

  factory :rspec_double, aliases: [:gitlab_response], class: 'RSpec::Mocks::Double' do
    skip_create

    initialize_with do
      new("Gitlab::ObjectifiedHash", **attributes)
    end
  end

  factory :comparison, aliases: [:compare], parent: :gitlab_response do
    commit { nil }
    commits { [] }
    compare_timeout { false }
    diffs { [] }
    web_url { "https://example.com/foo/bar/compare/foo...bar" }
  end

  factory :deployment, parent: :gitlab_response do
    id
    iid { id }
    ref { 'master' }
    sha
    status { 'success' }
    created_at
    updated_at { created_at }

    trait(:success)
    trait(:failed) { status { 'failed' } }
    trait(:running) { status { 'running' } }

    # Omnibus deployments are on auto-deploy tags
    factory :omnibus_deployment do
      ref { "42.1.#{Time.now.strftime('%Y%m%d%H')}+#{SecureRandom.hex(6)[0, 11]}.#{sha[0, 11]}" }
    end
  end

  factory :merge_request, parent: :gitlab_response do
    id
    iid { id }
    state { 'opened' }
    source_branch { 'feature-branch' }
    target_branch { 'master' }
    assignees { [] }
    labels { [] }
    web_url { "https://example.com/foo/bar/-/merge_requests/#{iid}" }

    trait(:closed) { state { 'closed' } }
    trait(:locked) { state { 'locked' } }
    trait(:merged) { state { 'merged' } }
    trait(:opened) { state { 'opened' } }
  end

  factory :pipeline, parent: :gitlab_response do
    id
    status { 'created' }
    ref { 'new-pipeline' }
    sha
    web_url { "https://example.com/foo/bar/-/pipelines/#{id}" }
    created_at
    updated_at { created_at }

    trait(:created) { status { 'created' } }
    trait(:waiting_for_resource) { status { 'waiting_for_resource' } }
    trait(:preparing) { status { 'preparing' } }
    trait(:pending) { status { 'pending' } }
    trait(:running) { status { 'running' } }
    trait(:success) do
      status { 'success' }
      finished_at
    end
    trait(:failed) do
      status { 'failed' }
      finished_at
    end
    trait(:canceled) { status { 'canceled' } }
    trait(:skipped) { status { 'skipped' } }
    trait(:manual) { status { 'manual' } }
    trait(:scheduled) { status { 'scheduled' } }
  end

  factory :user, parent: :gitlab_response do
    id
    name { 'GitLab User' }
    username { 'gitlab-user' }
  end
end
