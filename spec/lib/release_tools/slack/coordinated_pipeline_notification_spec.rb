# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Slack::CoordinatedPipelineNotification do
  let(:deploy_version) { '14.3.202108261121-1c87faa073d.2cec58b7eb5' }
  let(:deployer_url) { 'https://test.com/deployer/-/pipelines/123' }
  let(:environment) { 'gstg' }

  let(:pipeline) do
    create(
      :pipeline,
      :running,
      web_url: deployer_url,
      created_at: '2021-09-14T15:15:23.923Z'
    )
  end

  subject(:notifier) do
    described_class.new(
      deploy_version: deploy_version,
      pipeline: pipeline,
      environment: environment
    )
  end

  describe '#execute' do
    let(:local_time) { Time.utc(2021, 9, 14, 15, 55, 0) }

    let(:context_elements) do
      [
        { text: ':clock1: 2021-09-14 15:55 UTC', type: 'mrkdwn' },
        { text: ':sentry: <https://sentry.gitlab.net/gitlab/gitlabcom/releases/1c87faa073d/|View Sentry>', type: 'mrkdwn' }
      ]
    end

    before do
      allow(ReleaseTools::Slack::Message)
        .to receive(:post)
        .and_return({})
    end

    it 'sends a slack message' do
      block_content =
        ":building_construction: :ci_running: *gstg* <#{deployer_url}|started> `#{deploy_version}`"

      expect(ReleaseTools::Slack::Message)
        .to receive(:post)
        .with(
          channel: ReleaseTools::Slack::ANNOUNCEMENTS,
          message: "gstg started 14.3.202108261121-1c87faa073d.2cec58b7eb5",
          blocks: slack_mrkdwn_block(text: block_content, context_elements: context_elements)
        )

      without_dry_run do
        Timecop.freeze(local_time) do
          notifier.execute
        end
      end
    end

    context 'with a success pipeline' do
      let(:pipeline) do
        create(
          :pipeline,
          :success,
          web_url: deployer_url,
          created_at: '2021-09-14T15:15:23.923Z'
        )
      end

      it 'includes the correct status and the wall time' do
        block_content =
          ":building_construction: :ci_passing: *gstg* <#{deployer_url}|finished> `#{deploy_version}`"

        elements = context_elements.concat(
          [{ type: 'mrkdwn', text: ":timer_clock: 39 minutes" }]
        )

        expect(ReleaseTools::Slack::Message)
          .to receive(:post)
          .with(
            channel: ReleaseTools::Slack::ANNOUNCEMENTS,
            message: "gstg finished 14.3.202108261121-1c87faa073d.2cec58b7eb5",
            blocks: slack_mrkdwn_block(text: block_content, context_elements: elements)
          )

        without_dry_run do
          Timecop.freeze(local_time) do
            notifier.execute
          end
        end
      end
    end

    context 'with a failed pipeline' do
      let(:pipeline) do
        create(
          :pipeline,
          :failed,
          web_url: deployer_url,
          created_at: '2021-09-14T15:15:23.923Z'
        )
      end

      it 'includes the correct status and the wall time' do
        block_content =
          ":building_construction: :ci_failing: *gstg* <#{deployer_url}|failed> `#{deploy_version}`"

        elements = context_elements.concat(
          [{ type: 'mrkdwn', text: ":timer_clock: 39 minutes" }]
        )

        expect(ReleaseTools::Slack::Message)
          .to receive(:post)
          .with(
            channel: ReleaseTools::Slack::ANNOUNCEMENTS,
            message: "gstg failed 14.3.202108261121-1c87faa073d.2cec58b7eb5",
            blocks: slack_mrkdwn_block(text: block_content, context_elements: elements)
          )

        without_dry_run do
          Timecop.freeze(local_time) do
            notifier.execute
          end
        end
      end
    end

    context 'with a long deployment' do
      let(:pipeline) do
        create(
          :pipeline,
          :success,
          web_url: deployer_url,
          created_at: '2021-09-14T13:13:23.923Z'
        )
      end

      it 'includes the correct duration' do
        block_content =
          ":building_construction: :ci_passing: *gstg* <#{deployer_url}|finished> `#{deploy_version}`"

        elements = context_elements.concat(
          [{ type: 'mrkdwn', text: ":timer_clock: 2 hours" }]
        )

        expect(ReleaseTools::Slack::Message)
          .to receive(:post)
          .with(
            channel: ReleaseTools::Slack::ANNOUNCEMENTS,
            message: "gstg finished 14.3.202108261121-1c87faa073d.2cec58b7eb5",
            blocks: slack_mrkdwn_block(text: block_content, context_elements: elements)
          )

        without_dry_run do
          Timecop.freeze(local_time) do
            notifier.execute
          end
        end
      end
    end

    context 'with a different environment' do
      let(:environment) { 'gprd' }

      it 'displays the environment and specific emoji' do
        block_content =
          ":party-tanuki: :ci_running: *gprd* <#{deployer_url}|started> `#{deploy_version}`"

        expect(ReleaseTools::Slack::Message)
          .to receive(:post)
          .with(
            channel: ReleaseTools::Slack::ANNOUNCEMENTS,
            message: "gprd started #{deploy_version}",
            blocks: slack_mrkdwn_block(text: block_content, context_elements: context_elements)
          )

        without_dry_run do
          Timecop.freeze(local_time) do
            notifier.execute
          end
        end
      end
    end

    context 'when running in dry-run mode' do
      it 'does nothing' do
        expect(ReleaseTools::Slack::Message)
          .not_to receive(:post)

        notifier.execute
      end
    end
  end
end
