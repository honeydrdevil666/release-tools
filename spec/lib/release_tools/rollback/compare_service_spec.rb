# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Rollback::CompareService do
  let!(:fake_client) { stub_const('ReleaseTools::GitlabClient', spy) }

  def stub_deployments(deployments, environment = 'fake-production')
    allow(fake_client).to receive(:deployments)
      .with(described_class::PROJECT, environment)
      .and_return(deployments)
  end

  describe 'initialize' do
    it 'attempts to parse from packages' do
      current = attributes_for(:omnibus_deployment)[:ref]
      target = attributes_for(:omnibus_deployment)[:ref]

      instance = described_class.new(current: current, target: target)

      expect(instance.current.to_tag).to eq(current)
      expect(instance.target.to_tag).to eq(target)
    end
  end

  describe '#execute' do
    it 'fetches from environment' do
      deployments = [
        build(:omnibus_deployment, :failed),
        build(:omnibus_deployment, :success),
        build(:omnibus_deployment, :failed),
        build(:omnibus_deployment, :success)
      ]

      stub_deployments(deployments, 'gprd')

      instance = described_class.new(current: 'gprd', target: 'gprd')
      instance.execute

      expect(instance.current.to_tag).to eq(deployments[0].ref)
      expect(instance.target.to_tag).to eq(deployments[1].ref)
    end

    it 'compares a running deployment to the previous successful deployment' do
      deployments = [
        build(:omnibus_deployment, :failed),
        build(:omnibus_deployment, :running),
        build(:omnibus_deployment, :failed),
        build(:omnibus_deployment, :success)
      ]

      stub_deployments(deployments, 'gprd')

      instance = described_class.new(current: 'gprd', target: 'gprd')
      instance.execute

      expect(instance.current.to_tag).to eq(deployments[0].ref)
      expect(instance.target.to_tag).to eq(deployments[-1].ref)
    end

    it 'compares a current successful deployment to the previous successful deployment' do
      deployments = [
        build(:omnibus_deployment, :success),
        build(:omnibus_deployment, :failed),
        build(:omnibus_deployment, :success),
        build(:omnibus_deployment, :failed)
      ]

      stub_deployments(deployments, 'gprd')

      instance = described_class.new(current: 'gprd', target: 'gprd')
      instance.execute

      expect(instance.current.to_tag).to eq(deployments[0].ref)
      expect(instance.target.to_tag).to eq(deployments[2].ref)
    end

    it 'compares a package to the previous successful deployment' do
      deployments = [
        build(:omnibus_deployment, :success),
        build(:omnibus_deployment, :success)
      ]
      package = deployments.first.ref

      stub_deployments(deployments, 'gprd')

      instance = described_class.new(current: package, target: 'gprd')
      instance.execute

      expect(instance.current.to_tag).to eq(deployments[0].ref)
      expect(instance.target.to_tag).to eq(deployments[1].ref)
    end

    it "raises ArgumentError if deployments can't be retrieved" do
      deployments = [
        build(:omnibus_deployment, :failed),
        build(:omnibus_deployment, :failed)
      ]

      stub_deployments(deployments, 'gprd')
      expect(ReleaseTools::Rollback::Comparison).not_to receive(:new)

      instance = described_class.new(current: 'gprd', target: 'gprd')

      expect { instance.execute }.to raise_error(ArgumentError)
    end
  end
end
