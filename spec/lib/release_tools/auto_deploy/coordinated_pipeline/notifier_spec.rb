# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::AutoDeploy::CoordinatedPipeline::Notifier do
  let(:deploy_version) { '14.3.202108261121-1c87faa073d.2cec58b7eb5' }
  let(:environment) { 'gstg' }
  let(:fake_client) { stub_const('ReleaseTools::GitlabOpsClient', spy) }

  let(:fake_notification) do
    stub_const('ReleaseTools::Slack::CoordinatedPipelineNotification', spy)
  end

  subject(:notifier) do
    described_class.new(
      pipeline_id: 123,
      deploy_version: '14.3.202108261121-1c87faa073d.2cec58b7eb5',
      environment: environment
    )
  end

  describe '#execute' do
    context 'when downstream pipeline can be found' do
      let(:gstg_deployer_pipeline) do
        create(:pipeline, :running, web_url: 'https://test.gitlab.net/deployer/-/pipelines/123')
      end

      let(:cny_deployer_pipeline) do
        create(:pipeline, :running, web_url: 'https://test.gitlab.net/deployer/-/pipelines/456')
      end

      let(:gprd_deployer_pipeline) do
        create(:pipeline, :running, web_url: 'https://test.gitlab.net/deployer/-/pipelines/789')
      end

      let(:gstg_bridge) do
        create(
          :gitlab_response,
          name: 'auto_deploy:deploy:gstg',
          downstream_pipeline: gstg_deployer_pipeline
        )
      end

      let(:cny_bridge) do
        create(
          :gitlab_response,
          name: 'auto_deploy:deploy:gprd-cny',
          downstream_pipeline: cny_deployer_pipeline
        )
      end

      let(:gprd_bridge) do
        create(
          :gitlab_response,
          name: 'auto_deploy:deploy:gprd',
          downstream_pipeline: gprd_deployer_pipeline
        )
      end

      before do
        enable_feature(:single_pipeline_phase_2_slack_notification)

        allow(fake_client)
          .to receive(:pipeline_bridges)
          .and_return([gstg_bridge, cny_bridge, gprd_bridge])

        allow(fake_notification)
          .to receive(:new)
          .and_return(double(execute: nil))
      end

      it 'sends a slack notification' do
        expect(ReleaseTools::Slack::CoordinatedPipelineNotification)
          .to receive(:new)
          .with(
            deploy_version: deploy_version,
            pipeline: gstg_deployer_pipeline,
            environment: 'gstg'
          )

        without_dry_run do
          notifier.execute
        end
      end

      context 'with a different environment' do
        let(:environment) { 'gprd-cny' }

        it 'sends a slack notification' do
          expect(ReleaseTools::Slack::CoordinatedPipelineNotification)
            .to receive(:new)
            .with(
              deploy_version: deploy_version,
              pipeline: cny_deployer_pipeline,
              environment: 'gprd-cny'
            )

          without_dry_run do
            notifier.execute
          end
        end
      end
    end

    context "when downstream pipeline can't be found" do
      before do
        enable_feature(:single_pipeline_phase_2_slack_notification)

        allow(fake_client)
          .to receive(:pipeline_bridges)
          .and_return([])
      end

      it 'does nothing' do
        expect(ReleaseTools::Slack::CoordinatedPipelineNotification)
          .not_to receive(:new)

        without_dry_run do
          notifier.execute
        end
      end
    end

    context 'when feature flag is disabled' do
      it 'does nothing' do
        disable_feature(:single_pipeline_phase_2_slack_notification)

        expect(ReleaseTools::Slack::CoordinatedPipelineNotification)
          .not_to receive(:new)

        without_dry_run do
          notifier.execute
        end
      end
    end
  end
end
