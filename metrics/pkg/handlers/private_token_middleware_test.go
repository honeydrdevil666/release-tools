package handlers

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

type mockHandler struct {
	cnt int
}

func (m *mockHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	m.cnt += 1

	w.WriteHeader(http.StatusOK)
}

func TestPrivateTokenMiddleware(t *testing.T) {
	expectedToken := "a token"
	wrongToken := "not a valid token" //nolint:gosec
	middleware := PrivateTokenMiddleware(expectedToken)

	examples := []struct {
		name       string
		token      *string
		authorized bool
	}{
		{
			name:       "no token",
			token:      nil,
			authorized: false,
		},
		{
			name:       "wrong token",
			token:      &wrongToken,
			authorized: false,
		},
		{
			name:       "good token",
			token:      &expectedToken,
			authorized: true,
		},
	}

	for _, example := range examples {
		t.Run(example.name, func(tt *testing.T) {
			innerHandler := &mockHandler{}

			req, err := http.NewRequest("GET", "/", nil)
			if err != nil {
				tt.Fatalf("Cannot init a new HTTP request. %v", err)
			}

			if example.token != nil {
				req.Header.Set("X-Private-Token", *example.token)
			}

			rr := httptest.NewRecorder()
			middleware(innerHandler).ServeHTTP(rr, req)

			expectedStatus := http.StatusUnauthorized
			expectedCnt := 0
			if example.authorized {
				expectedStatus = http.StatusOK
				expectedCnt = 1
			}

			if status := rr.Code; status != expectedStatus {
				tt.Errorf("handler returned wrong status code: got %v want %v",
					status, expectedStatus)
			}

			if innerHandler.cnt != expectedCnt {
				tt.Errorf("Handler processed %v times, expected %v", innerHandler.cnt, expectedCnt)
			}
		})
	}
}
